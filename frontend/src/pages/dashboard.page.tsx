import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers(page);
      setLoading(false);
      setUsers(result.data);
    };

    fetchData();
  }, [page]);

  const handleClick = (operation: "subtract" | "add") => {
    if (operation === "subtract") {
      setPage(prevState => {
        if (prevState - 1 === 0) return prevState;
        return prevState - 1;
      });
      return;
    }
    setPage(prevState => prevState + 1);
  };

  return (
    <div style={{ paddingTop: "30px", display: "flex" }}>
      <div className="pages">
        <button onClick={() => handleClick("subtract")}>{"<"}</button>
        <div>{page}</div>
        <button onClick={() => handleClick("add")}>{">"}</button>
      </div>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map(user => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
